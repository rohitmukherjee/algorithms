"""
You are a given a unimodal array of n distinct elements, meaning that its entries are in increasing order up until its maximum element, after which its elements are in decreasing order. Give an algorithm to compute the maximum element that runs in O(log n) time.
"""

def find_maximum_trivial(array):
    maximum = array[0]
    for element in array:
        if element > maximum:
            maximum = element
    return maximum

def find_maximum(array):
   high = len(array)
   low = 0
   while low <= high:
       mid = int((low + high) * 0.5)
       # Found our max element
       if array[mid] > array[mid - 1] and array[mid] > array[mid+1]:
           return array[mid]
       # Too far right
       elif array[mid] < array[mid - 1] and array[mid] > array[mid+1]:
           high = mid
        # Too far left
       else:
           low = mid

def run():
    array = [1, 2, 3, 4, 8, 7, 6, 5, 4.2, 3.2, 1.5, 1.4, 1.3, 1.1, 0.9]
    array2 = [1, 8, 7, 6, 5, 4, 3, 2]
    print(find_maximum_trivial(array))
    print(find_maximum(array))

run()
