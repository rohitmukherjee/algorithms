import unittest


class BST:
    def __init__(self, parent=None):
        self.value = None
        self.left = None
        self.right = None
        self.parent = parent

    def get_terminal_node(self, tgt):
        if self.left is None: self.left = BST(parent=self)
        if self.right is None: self.right = BST(parent=self)

        if self.value == tgt or self.value is None:
            return self
        if tgt < self.value:

            return self.left.get_terminal_node(tgt)
        else:
            return self.right.get_terminal_node(tgt)

    def search(self, tgt):
        node = self.get_terminal_node(tgt)
        return node.value == tgt

    def insert(self, tgt):
        node = self.get_terminal_node(tgt)
        node.value = tgt
        return node

    def min(self):
        while self.left is not None:
            self = self.left
        return self

    def max(self):
        while self.right is not None:
            self = self.right
        return self

    #     15
    #  12    18

    def predecessor(self):
        if not self:
            return None
        if self.left:
            return self.left.max()
        else:
            curr = self
            while curr and curr.parent and curr.parent.value > curr.value:
                curr = curr.parent
            return curr.parent

    def successor(self, cur):
        if self.right:
            return self.right.max()

    def __str__(self):
        return f"<BST Node: {self.value}>"


class BSTTest(unittest.TestCase):
    def test_min_max(self):
        root = BST()
        root.value = 15
        left = BST()
        left.value = 12
        right = BST()
        right.value = 18
        root.left = left
        root.right = right
        left.parent = root

        right.parent = root
        self.assertEqual(12, root.min().value)
        self.assertEqual(18, root.max().value)

    def test_predecessor_simple(self):
        root = BST()
        root.value = 15

        left = BST()
        left.value = 12
        root.left = left
        left.parent = root

        right = BST()
        root.right = right
        right.value = 18
        right.parent = root

        self.assertEqual(None, left.predecessor())
        self.assertEqual(12, root.predecessor().value)
        self.assertEqual(15, right.predecessor().value)

    def test_predecessor_larger_tree(self):
        root = BST()
        root.value = 15
        left = BST()
        left.value = 12

        left_left = BST()
        left_left.value = 11
        left.left = left_left
        left_left.parent = left

        left_right = BST()
        left_right.value = 14
        left.right = left_right
        left_right.parent = left

        left_right_left = BST()
        left_right_left.value = 13
        left_right.left = left_right_left
        left_right_left.parent = left_right

        right = BST()
        right.value = 18
        root.left = left
        root.right = right
        left.parent = root
        right.parent = root

        right_left = BST()
        right_left.value = 17
        right.left = right_left
        right_left.parent = right

        right_right = BST()
        right_right.value = 19
        right.right = right_right
        right_right.parent = right

        right_right_right = BST()
        right_right_right.value = 22
        right_right.right = right_right_right
        right_right_right.parent = right_right

        right_right_right_left = BST()
        right_right_right_left.value = 21
        right_right_right.left = right_right_right_left
        right_right_right_left.parent = right_right_right

        self.assertEqual(11, left.predecessor().value)
        self.assertIsNone(left_left.predecessor())
        self.assertEquals(13, left_right.predecessor().value)
        self.assertEquals(19, right_right_right_left.predecessor().value)
