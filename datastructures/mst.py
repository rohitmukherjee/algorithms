# Union Find Disjoint Set
import unittest


class UnionFindDisjointSet:
    def __init__(self, n: int):
        self.n = n
        self.p = []
        self.rank = {}
        for i in range(0, n):
            self.p.append(i)
            self.rank[i] = 0

    def find(self, i) -> int:
        current = i
        while current != self.p[current]:
            current = self.p[current]
        # Path-compression heuristic to shorten paths:
        self.p[i] = current
        return current

    def isSameSet(self, i, j) -> bool:
        return i == j or self.find(i) == self.find(j)

    def union(self, i, j) -> int:
        rank_i = self.rank[i]
        rank_j = self.rank[j]
        if rank_i > rank_j:
            self.p[j] = i
        else:
            self.p[i] = j
            if self.rank[i] == self.rank[j]:
                self.rank[j] += 1

#
# class UnionFindDisjointSetTest(unittest.TestCase):
#     def testFind(self):
#         ufds = UnionFindDisjointSet(10)
#         self.assertEquals(1, ufds.find(1))
#         self.assertEquals(2, ufds.find(2))
#         ufds.union(1, 2)
#         ufds.union(3, 2)
#         ufds.union(5, 2)
#         self.assertEquals(2, ufds.find(2))
#         self.assertEquals(2, ufds.find(1))
#         self.assertEquals(2, ufds.find(3))
#         self.assertEquals(2, ufds.find(5))


edges = open("edges.txt", "r").read()
l = []
for i in edges.split("\n"):
    nl = i.split()
    nl[0] = int(nl[0]) - 1
    nl[1] = int(nl[1]) - 1
    nl[2] = int(nl[2])
    l.append(nl)

number_of_nodes = 500
d = UnionFindDisjointSet(number_of_nodes)
l = sorted(l, key=lambda x: x[2])
edges = 0
edges_sum = 0

for i in l:
    if not d.isSameSet(i[0], i[1]):
        d.union(i[1], i[0])
        edges += 1
        edges_sum += i[2]
    # if edges == number_of_nodes - 1:
    #     break


print("kruskals", edges_sum)
print("prims", -3612829)
