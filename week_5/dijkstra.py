#!/usr/bin/python
'''
Your task is to run Dijkstra's shortest-path algorithm on this graph, using 1 (the first vertex) as the source vertex, 

and to compute the shortest-path distances between 1 and every other vertex of the graph. 

If there is no path between a vertex v and vertex 1, we'll define the shortest-path distance between 1 and v to be 1000000. 

You should report the shortest-path distances to the following ten vertices, 

in order: 7,37,59,82,99,115,133,165,188,197. 

You should encode the distances as a comma-separated string of integers. So if you find that all ten of these vertices except 115 are at distance 1000 away from vertex 1 and 115 is 2000 distance away, 

then your answer should be 1000,1000,1000,1000,1000,2000,1000,1000,1000,1000. 

Please type your answer in the space provided.

IMPLEMENTATION NOTES: This graph is small enough that the straightforward O(mn) time implementation of Dijkstra's algorithm should work fine. 

OPTIONAL: For those of you seeking an additional challenge, try implementing the heap-based version. 

Note this requires a heap that supports deletions, and you'll probably need to maintain some kind of mapping between vertices and their positions in the heap.
'''
from collections import defaultdict
import Queue as Q

INFINITY = int(10 ** 20)

RESULT_KEYS = [7, 37, 59, 82, 99, 115, 133, 165, 188, 197]
TEST_KEYS = [10, 30, 50, 80, 90, 110, 130, 160, 180, 190]


class Edge:
    ''' To model an edge length and destination vertex'''
    vertex = 0
    length = 0

    def __init__(self, _vertex, _length):
        self.vertex = _vertex
        self.length = _length

    def __repr__(self):
        return str(self.vertex) + "|" + str(self.length)


def read_input():
    graph_string = open("dijkstraData.txt").readlines()
    global adjacency_list
    global number_of_nodes
    number_of_nodes = 0
    adjacency_list = defaultdict(list)
    for line in graph_string:
        split_line = line.split('\t')
        current_vertex = int(split_line[0])
        if current_vertex > number_of_nodes:
            number_of_nodes = current_vertex
        split_line = split_line[1:-1]
        for element in split_line:
            element_parsed = element.split(',')
            edge = Edge(int(element_parsed[0]), int(element_parsed[1]))
            if edge.vertex > number_of_nodes:
                number_of_nodes = edge.vertex
            adjacency_list[int(current_vertex)].append(edge)
    print(adjacency_list)


def dijkstra(adjacency_list, source_vertex):
    distance = defaultdict()
    for vertex in range(0, number_of_nodes + 1):
        distance[int(vertex)] = INFINITY
    distance[int(source_vertex)] = 0
    q = Q.PriorityQueue()
    q.put((0, source_vertex))
    while not q.empty():
        (length, current_vertex) = q.get()
        edges = adjacency_list[int(current_vertex)]
        print("processing vertex %s " % current_vertex)
        print("number of outgoing edges %s" % len(edges))
        for edge in edges:
            print("Looking at vertex %s %s" % (edge.vertex, edge.length))
            if distance[edge.vertex] >= (distance[current_vertex] + edge.length):
                distance[edge.vertex] = (distance[current_vertex] + edge.length)
                q.put((distance[edge.vertex], edge.vertex))
            else:
                pass
    return distance


def run():
    read_input()
    distance = dijkstra(adjacency_list, 1)
    results = []
    for key in RESULT_KEYS:
        results.append(distance[int(key)])
    print(results)


run()
