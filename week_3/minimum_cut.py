#!/usr/bin/python
import copy
from collections import defaultdict
from random import randint

def read_input():
	global adj_list
	adj_list = defaultdict(list)
	list_input = open("kargerMinCut.txt").readlines()
	for line in list_input:
		edges = line.split("\t")
		# edges = line.split(",")
		start_vertex = int(edges[0])
		# Only in place to remove the start_vertex and white space characters such as \r and \n
		edges = edges[1:-1]
		for end_vertex in edges:
			adj_list[start_vertex].append(int(end_vertex))

def generate_random_edge(adjacency_list):
	random_start_vertex = randint(1, 200)
	while random_start_vertex not in adjacency_list.keys():
		random_start_vertex = randint(1, 200)
	number_of_outgoing_edges = len(adjacency_list[random_start_vertex])
	random_end_vertex = adjacency_list[random_start_vertex][randint(0, number_of_outgoing_edges - 1)]
	if random_end_vertex not in adjacency_list.keys():
		raise("Inconsistent", "vertex")
	return (random_start_vertex, random_end_vertex)

def collapse_vertices(adjacency_list):
	(start_vertex, end_vertex) = generate_random_edge(adjacency_list)
	# print(start_vertex,end_vertex)
	if start_vertex is end_vertex:
		raise ("start and end", "are the same")
	# Remove edge
	adjacency_list[start_vertex].remove(end_vertex)
	adjacency_list[end_vertex].remove(start_vertex)
	# Get all outgoing edges from second end_vertex
	outgoing_edges_from_end_vertex = adjacency_list[end_vertex]
	# print(outgoing_edges_from_end_vertex)
	# Add those end_vertices to the start_vertex
	for vertex in outgoing_edges_from_end_vertex:
		# Guarding against self loops
		adjacency_list[vertex].remove(end_vertex)
		if start_vertex is not vertex:
			adjacency_list[start_vertex].append(vertex)
			adjacency_list[vertex].append(start_vertex)
	# print("****")
	del adjacency_list[end_vertex]
	adjacency_list.pop(end_vertex, None)

def random_contraction(adjacency_list):
	while len(adjacency_list.keys(	)) > 2:
		collapse_vertices(adjacency_list)
	return len(adjacency_list[adjacency_list.keys()[0]])

def min_cut():
	global min_cut
	min_cut = 2000
	number_of_iterations = (200 * 200 * 10)
	for count in range(0, number_of_iterations):
		cut = random_contraction(copy.deepcopy(adj_list))
		print(cut)
		if  cut < min_cut:
			min_cut = cut
		count += 1
	return min_cut

read_input()
print(min_cut())