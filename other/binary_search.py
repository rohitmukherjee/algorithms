import unittest


def binary_search(l: [], k: int) -> int:
    if len(l) == 0:
        return -1
    min_index = 0
    max_index = len(l) - 1
    while min_index <= max_index <= len(l) - 1:
        mid = int((min_index + max_index) / 2)
        if l[mid] > k:
            max_index = mid - 1
        elif l[mid] < k:
            min_index = mid + 1
        else:
            return mid
    return -1


def binary_search_closest_lower_match(l: [], k: int) -> int:
    if len(l) == 0:
        return -1
    min_index = 0
    max_index = len(l) - 1
    prev_diff_index = -1
    while min_index <= max_index <= len(l) - 1:
        mid = int((min_index + max_index) / 2)
        if l[mid] > k:
            max_index = mid - 1
        elif l[mid] < k:
            if prev_diff_index == -1:
                prev_diff_index = mid
            elif k - l[mid] < l[prev_diff_index]:
                prev_diff_index = mid
            min_index = mid + 1
        else:
            return mid
    return prev_diff_index


class BinarySearchTest(unittest.TestCase):
    def test_binary_search(self):
        self.assertEqual(-1, binary_search([], 1))
        self.assertEqual(-1, binary_search([2], 1))
        self.assertEqual(0, binary_search([1], 1))
        self.assertEqual(3, binary_search([2, 3, 4, 5], 5))
        self.assertEqual(-1, binary_search([2, 3, 4, 5], 6))
        self.assertEqual(2, binary_search([2, 3, 4, 5], 4))
        self.assertEqual(2, binary_search([2, 3, 9, 10, 11], 9))
        self.assertEqual(0, binary_search([2, 3, 19, 24, 31], 2))

    def test_binary_search_closest_lower_match(self):
        self.assertEqual(3, binary_search_closest_lower_match([2, 3, 4, 5], 6))
        self.assertEqual(3, binary_search_closest_lower_match([2, 3, 4, 6], 7))
        self.assertEqual(3, binary_search_closest_lower_match([2, 3, 4, 6], 7))
        self.assertEqual(-1, binary_search_closest_lower_match([2, 3, 4, 5], 1))
