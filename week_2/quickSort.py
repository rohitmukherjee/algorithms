def read_input():
    global array
    global number_of_comparisons
    number_of_comparisons = 0
    array = []
    array2 = []
    array2 = open("QuickSort.txt").readlines()
    # print("number of element: " + len(array2))
    for i in array2:
        array.append(int(i))

def quickSort(array, start, end):
    if start > end:
        return
    pivot_index = median_of_three_pivot(array, start, end)
    k = array[start]
    array[start] = array[pivot_index]
    array[pivot_index] = k

    partition_index = partition(array, start, end)
    print("partition_index: " + str(partition_index))
    quickSort(array, start, partition_index - 1)
    quickSort(array, partition_index + 1, end)

def partition(array, l, r):
    global number_of_comparisons
    print(number_of_comparisons)
    p = array[l]
    i = l + 1
    for j in range (l + 1, r + 1):
        number_of_comparisons += 1
        if array[j] < p:
            k = array[i]
            array[i] = array[j]
            array[j] = k
            i = i + 1
    k = array[l]
    array[l] = array[i - 1]
    array[i - 1] = k
    return i - 1

def median_of_three_pivot(array, start, end):
    length = end - start + 1
    if length <= 1:
        return start
    middle = (start + end )/ 2
    pivots = []
    pivots.append(array[start])
    pivots.append(array[middle])
    pivots.append(array[end])
    pivots.sort()
    return array.index(pivots[1])

read_input()
small_array = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
# small_array = [9, 8, 7, 5, 6, 2, 3, 4, 1, 0, 34, 56, 8, 23, 12, 34, 56, 8, 98, 45, 12, 46, 68, 98, 234,57, 9, 346, 68,468, 134, 4671,356, 758, 245,32, 356]
quickSort(array, 0, len(array) - 1)
# median_of_three_pivot(small_array, 0, 9)
print("final: " + str(number_of_comparisons))
# partition(small_array, 2, 8)
print(small_array)
