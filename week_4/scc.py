#!/usr/bin/python
from collections import defaultdict
from itertools import groupby
import sys
def read_input():
	graph_string = open("small.txt").readlines()
	global adjacency_list
	global total_vertices
	global adjacency_list_transpose
	global visited
	vertices_order = []
	adjacency_list = defaultdict(list)
	adjacency_list_transpose = defaultdict(list)
	for line in graph_string:
		split_line = line.split(' ')
		adjacency_list[int(split_line[0])].append(int(split_line[1]))
		adjacency_list_transpose[int(split_line[1])].append(int(split_line[0]))
	total_vertices = 875714
	visited = defaultdict(bool)
	for i in range(1, total_vertices + 1):
		visited[i] = False

def dfs(graph, start_vertex):
	global visited
	global leader
	global t
	global f
	global s
	visited[start_vertex] = True
	leader[start_vertex] = s
	neighbours = graph[start_vertex]
	for neighbour in neighbours:
			if not visited[neighbour]:
				dfs(graph, neighbour)
	t += 1
	f[start_vertex] = t

def cc():
	read_input()
	global visited
	global leader
	global t
	global s
	global f
	t = 0
	f = defaultdict(int)
	leader = defaultdict(int)
	vertices = []

	for vertex in range(1, total_vertices + 1):
		if not visited[vertex] and len(adjacency_list_transpose[vertex]) > 0:
			print("new call to dfs from: " + str(vertex))
			s = vertex
			dfs(adjacency_list_transpose, vertex)

	# Reset visited list
	for i in range(1, total_vertices + 1):
		visited[i] = False

	print("Finishing Times")
	print(f.values())
	# Create a sorted tuple based on finishing times
	for vertex in f.keys():
		vertices.append((vertex, f[vertex]))
	vertices.sort(key = lambda x: x[1], reverse = True)
	vertices_list = []
	for vertex in vertices:
		vertices_list.append(vertex[0])

	# print(vertices_list)
	leader = defaultdict()
	f = defaultdict()
	print(vertices_list)
	for vertex in vertices_list:
		if not visited[vertex]:
			print("exploring %s" % str(vertex))
			s = vertex
			dfs(adjacency_list, vertex)
	components = defaultdict(int)
	for lead in leader.values():
		if components[lead]:
			components[lead] = components[lead] + 1
		else:
			components[lead] = 1
	components_sorted = []
	for component in components:
		components_sorted.append(components[component])
	components_sorted.sort(reverse=True)
	print(components_sorted[0:5])

sys.setrecursionlimit(10 ** 7)
cc()
# read_input()
# print(adjacency_list)
