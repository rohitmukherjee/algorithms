import heapq


class MedianMaintenance:
    # The central idea here is that:
    # 1. All the elements in the maxHeap are <= the median of the data set
    # 2. All the elements in the minHeap are >= the median of the data set
    def __init__(self):
        self.minHeap, self.maxHeap = [], []
        self.n = 0

    def insert(self, num):
        if len(self.minHeap) == len(self.maxHeap):
            # The default implementation of `heapq` uses min-heap semantics
            # Multiplying each of the numbers by -1 while inserting into the maxHeap
            # and then finally multiplying by -1 when performing a peek() lets us use `heapq` with max-heap
            # semantics. Bear in mind that this only works if you know that all your input elements are going to be
            # non-negative.
            heapq.heappush(self.maxHeap, -1 * num)
            self.n += 1
            # If this is the first element we're inserting, don't insert move the element to the minHeap
            # let it stay in the max heap.
            if len(self.minHeap) == 0:
                return
            # If the top of the minHeap is less than the top of the max heap, it means our invariant (stated above)
            # doesn't hold and therefore we need to swap the top of the minHeap with the top of the maxHeap.
            if -1 * self.maxHeap[0] > self.minHeap[0]:
                toMin = -1 * heapq.heappop(self.maxHeap)
                toMax = heapq.heappop(self.minHeap)
                heapq.heappush(self.maxHeap, -1 * toMax)
                heapq.heappush(self.minHeap, toMin)
        else:
            # If there are an unequal number of elements in the min & max heaps, this means that the maxHeap has one
            # more element (as we're biased towards the maxHeap in our insert function). So we insert it into the
            # minHeap (after passing it through the maxHeap once, to get the "max" element).
            toMin = -1 * heapq.heappushpop(self.maxHeap, -1 * num)
            heapq.heappush(self.minHeap, toMin)
            self.n += 1

    def get_median(self):
        # if self.n%2 == 0:
        # 	return (-1*self.maxHeap[0]+self.minHeap[0])/2.0
        # else:
        return -1 * self.maxHeap[0]


def run():
    medians = []
    median_sum = 0
    heaps = MedianMaintenance()
    numbers = open("median_data.txt").readlines()
    for number in numbers:
        number = int(number.strip())
        heaps.insert(number)
        median = heaps.get_median()
        medians.append(median)
        median_sum += median
    return median_sum % 10000


class Heap:
    def __init__(self):
        self.heap = []

    def peek(self):
        if len(self.heap) == 0:
            return None
        return self.heap[0]

    def insert(self, number):
        self.heap.insert(0, number)
        self.heapify()

    def pop(self):
        if len(self.heap) == 0:
            return None
        self.heap[0], self.heap[-1] = self.heap[-1], self.heap[0]
        toreturn = self.heap.pop()
        self.heapify()
        return toreturn

    def __len__(self):
        return len(self.heap)

    def get_len(self):
        return len(self)

    def heapify(self):
        curr_idx = 0
        while curr_idx < len(self.heap) // 2:  # while there are children
            left_idx = (2 * curr_idx) + 1
            right_idx = (2 * curr_idx) + 2
            # know left exists for sure...
            if right_idx >= len(self.heap):  # if right does not exist...
                # then use left child to swap
                swap_idx = left_idx
            elif self.comparison(self.heap[right_idx], self.heap[left_idx]):
                swap_idx = right_idx
            else:
                swap_idx = left_idx
            # now swap, only if there is a violation of max heap property
            if self.comparison(self.heap[swap_idx], self.heap[curr_idx]):
                self.heap[swap_idx], self.heap[curr_idx] = self.heap[curr_idx], self.heap[swap_idx]
                curr_idx = swap_idx
            else:  # nothing to swap, can stop heapifying
                return

    def comparison(self, v1, v2):
        raise Exception("To be implemented")


# Zara's implementation
class MinHeap(Heap):
    def comparison(self, v1, v2):
        return v1 < v2


class MaxHeap(Heap):
    def comparison(self, v1, v2):
        return v1 > v2


class MedianTracker:
    def __init__(self):
        self.min_heap = MinHeap()
        self.max_heap = MaxHeap()

    def get_num_of_submissions(self):
        return self.min_heap.get_len() + self.max_heap.get_len()

    def insert(self, number):
        if self.min_heap.get_len() == self.max_heap.get_len():
            self.max_heap.insert(number)
            self.min_heap.insert(self.max_heap.pop())
        else:
            self.min_heap.insert(number)
            self.max_heap.insert(self.min_heap.pop())

    def get_median(self):
        # If size of min heap == size of max heap
        if self.min_heap.get_len() == self.max_heap.get_len():
            return (self.min_heap.peek() + self.max_heap.peek()) / 2
        else:
            return self.min_heap.peek()


tracker = MedianTracker()

print(run())
