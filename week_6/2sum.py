#!/usr/bin/python
from collections import defaultdict

MIN = -10000L
MAX = 10000L
# MIN = -4L
# MAX = 4L

def read_input():
	numbers = defaultdict(long)
	numbers_map = defaultdict(list)
	count = 0
	numbers_string = open("data.txt").readlines()
	for number in numbers_string:
		number = number.strip()
		key = long(number)/MAX
		if long(number) not in numbers:
			numbers[long(number)] = long(number)
			numbers_map[key].append(long(number))
	seen = defaultdict(long)
	print(numbers_map)
	for number in numbers:
		# print(number)
		minimum = -1 * (number + MAX)/MAX
		maximum = (MAX - number)/MAX
		for x in range(minimum, maximum + 1):
			if x in numbers_map:
				# print(numbers_map[x])
				for y in numbers_map[x]:
					if ((number + y) >= MIN and (number + y) <= MAX and number != y and number + y not in seen):
						print(number, "+", y, "=", number + y)
						seen[number + y] = number
						count += 1
	return count

print(read_input())
# print(two_sum(-10000, 10000, numbers))