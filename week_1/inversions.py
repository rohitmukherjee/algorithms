def read_input():
	global array
	array = []
	array2 = []
	array2 = open("IntegerArray.txt").readlines()
	for i in array2:
		array.append(int(i))

def count_inversions(array, size):
	if size == 1:
		return 0
	else:
		first_half = get_array_first_half(array, size)
		second_half = get_array_second_half(array, size)
		x = count_inversions(first_half, len(first_half))
		y = count_inversions(second_half, len(second_half))
		first_half.sort()
		second_half.sort()
		z = count_split_inversions(first_half, second_half, size)
		return (x + y + z)

def count_split_inversions(first_half, second_half, size):
	count = 0
	first_half_index = 0
	second_half_index = 0
	final_array = []
	if first_half == None or second_half == None:
		return 0
	while first_half_index < len(first_half) and second_half_index < len(second_half):
		if first_half[first_half_index] <= second_half[second_half_index]:
			final_array.append(first_half[first_half_index])
			first_half_index += 1
		else:
			final_array.append(second_half[second_half_index])
			second_half_index += 1
			count += len(first_half[first_half_index:])
    final_array.extend(first_half[first_half_index:])
	final_array.extend(second_half[second_half_index:])
	return count

def count_inversions_naive(array, size):
	count = 0
	for i in range(0, size):
		for j in range(i + 1, size):
			if array[i] > array[j]:
				count = count + 1
	return count

def get_array_first_half(array, size):
	return array[0:size/2]

def get_array_second_half(array, size):
	return array[size/2:]

read_input()
# print("naive: " + str(count_inversions_naive(array, len(array))))
print("recursive: " + str(count_inversions(array, len(array))))
